job "metric-management" {
  type = "service"
  region = "global"
  datacenters = ["proxima"]

  group "metrics" {
    count = 1

    network {
      mode = "bridge"
    }

    service {
      name = "prometheus"
      port = "9090"

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "network-proxy-metrics"
              local_bind_port = 10000
            }
          }
        }
      }

      tags = [
        "traefik.enable=true",
        "traefik.consulcatalog.connect=true",
        "traefik.http.routers.prometheus.entrypoints=https",
        "traefik.http.routers.prometheus.tls=true",
        "traefik.http.routers.prometheus.tls.certresolver=lets-encrypt",
        "traefik.http.routers.prometheus.tls.domains[0].main=*.hq.carboncollins.se"
      ]
    }

    task "prometheus" {
      driver = "docker"

      config {
        image = "[[ .prometheusImage ]]"

        volumes = [
          "local/prometheus.yml:/etc/prometheus/prometheus.yml"
        ]
      }


      template {
        data = <<EOH
[[ fileContents "./config/prometheus.template.yml" ]]
        EOH

        destination = "local/prometheus.yml"
        change_mode = "noop"
      }
    }
  }
}
